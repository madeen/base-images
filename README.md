# Madeen.be base images

Repository of base images for [madeen.be](https://www.madeen.be) based projects.

All images are built daily in order to fetch the latest base image changes. 

Currently contains:
 * Nginx 1.18 (alpine)
 * PHP-FPM 7.4 (alpine)
 * Node 14.3 (alpine)
